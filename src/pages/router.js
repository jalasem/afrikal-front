import Home from '@/pages/home'

export default [
  {
    path: '/',
    name: 'Afrikal | Home',
    component: Home
  },
  {
    path: '*',
    component: Home
  }
]
