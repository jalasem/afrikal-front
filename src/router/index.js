import Vue from 'vue'
import Router from 'vue-router'

import Routes from '@/pages/router.js'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: Routes
})
